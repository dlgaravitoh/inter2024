package com.endava.inter2024api.test.pokemon;

import com.endava.inter2024api.api.config.RestAssuredConfigurator;
import com.endava.inter2024api.api.response.impl.PokeResponse;
import com.endava.inter2024api.api.service.impl.PokeAPI;
import com.endava.inter2024api.api.validation.ValidationStrategy;
import com.endava.inter2024api.api.validation.ValidationType;
import com.endava.inter2024api.api.validation.impl.PokeAssert;
import com.endava.inter2024api.dto.AbilityDetail;
import com.endava.inter2024api.dto.Pokemon;
import com.endava.utils.ConfigManager;
import com.endava.utils.DataReader;
import io.restassured.response.Response;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.endava.inter2024api.api.validation.ValidationType.*;
import static com.endava.inter2024api.constants.TestDataConstants.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class SpeciesIT {

    private static PokeAPI pokeAPI;
    private ValidationStrategy validator = new PokeAssert();
    private final DataReader pokeData = new DataReader(ConfigManager.getProperty("getPKTestData"));

    @BeforeAll
    public static void setup(){
        pokeAPI = new PokeAPI();
        RestAssuredConfigurator.configure();
    }

    @Test
    public void pokemonHasARealAbility_Success(){
        String specie="sprigatito";
        String ability="overgrow";
        String type="grass";

        Response specieResponse = given().baseUri("https://pokeapi.co/api/v2/").when().get("pokemon/"+specie);
        Pokemon pokemon = specieResponse.as(Pokemon.class);
        specieResponse.then()
                .assertThat()
                    .statusCode(200)
                .body("name", equalTo(specie))
                .body("is_default", equalTo(true))
                .body("height", greaterThan(0))
                .body("abilities.ability.name", hasItem(ability))
                .body("types.type.name", hasItem(type))
                .body("stats[0]", hasKey("base_stat"))
                .body("species", hasEntry("name", "sprigatito"))
                .body("weight", both(greaterThan(10)).and(lessThan(100)));

        given().baseUri("https://pokeapi.co/api/v2/").when().get("ability/"+pokemon.getAbilities().get(0).getAbility().getName())
                .then().assertThat().statusCode(200)
                .body("pokemon.pokemon.name", hasItem(specie));
    }

    @Test
    public void pokemonHasARealAbility_SuccessEX(){
        Pokemon pkmn = pokeData.getData(SPRIGATITO, Pokemon.class);
        String specie = pkmn.getName();
        String ability = pkmn.getAbilities().get(0).getAbility().getName();
        String type = pkmn.getTypes().get(0).getType().getName();

        PokeResponse<Pokemon> pokemonResponse = pokeAPI.getPokemon(specie);
        validator.status(pokemonResponse,200);
        validator.validate(pokemonResponse,NAME,EQUALS,specie);
        validator.validate(pokemonResponse,HEIGHT,GREATER_THAN,0);
        validator.validate(pokemonResponse,ABILITY_NAME,HAS_ITEM,ability);
        validator.validate(pokemonResponse,PK_TYPE_NAME,HAS_ITEM,type);
        validator.validate(pokemonResponse,STATS_INDEX0,HAS_KEY,BASE_STAT);
        validator.validate(pokemonResponse,PK_SPECIES,HAS_ENTRY,Map.entry(NAME, specie));
        validator.validate(pokemonResponse,WEIGHT,BOTH_GREATER_AND_LESS,Map.entry(10, 100));

        PokeResponse<AbilityDetail> abilityResponse = pokeAPI.getAbility(pokemonResponse.getData().getAbilities().get(0).getAbility().getName());
        validator.status(abilityResponse,200);
        validator.validate(abilityResponse,PK_NAME,HAS_ITEM,specie);
    }

    @Test
    public void pokemonHasARealAbility_SuccessEX2() {
        String specie = "sprigatito";
        PokeResponse<Pokemon> pokemonResponse = pokeAPI.getPokemon(specie);
        validator.validateAll(pokemonResponse,
                response -> validator.validate(pokemonResponse,NAME,EQUALS,specie),
                response -> validator.validate(pokemonResponse,HEIGHT,GREATER_THAN,0),
                response -> validator.validate(pokemonResponse,STATS_INDEX0,HAS_KEY,BASE_STAT),
                response -> validator.validate(pokemonResponse,PK_SPECIES,HAS_ENTRY,Map.entry(NAME, specie))
        );
    }

    @AfterEach
    public void teardown(){

    }
}
