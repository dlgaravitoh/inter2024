package com.endava.inter2024api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pokemon {
    private List<AbilityWrapper> abilities;
    private int baseExperience;
    private Cries cries;
    private List<Form> forms;
    private List<GameIndex> gameIndices;
    private int height;
    private List<Item> heldItems;
    private int id;
    private boolean isDefault;
    private String locationAreaEncounters;
    private List<MoveWrapper> moves;
    private String name;
    private int order;
    private Species species;
    private Sprites sprites;
    private List<StatWrapper> stats;
    private List<TypeWrapper> types;
    private int weight;

}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
class Cries {
    private String latest;
    private String legacy;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
class Form {
    private String name;
    private String url;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class GameIndex {
    private int gameIndex;
    private Version version;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class Version {
    private String name;
    private String url;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class Item {
    private ItemDetail item;
    private List<VersionDetail> versionDetails;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class ItemDetail {
    private String name;
    private String url;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class VersionDetail {
    private Version version;
    private int rarity;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class MoveWrapper {
    private Move move;
    private List<VersionGroupDetail> versionGroupDetails;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class Move {
    private String name;
    private String url;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class VersionGroupDetail {
    private int levelLearnedAt;
    private MoveLearnMethod moveLearnMethod;
    private VersionGroup versionGroup;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class MoveLearnMethod {
    private String name;
    private String url;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class VersionGroup {
    private String name;
    private String url;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class Species {
    private String name;
    private String url;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class Sprites {
    private String frontDefault;
    private OtherSprites other;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class OtherSprites {
    private OfficialArtwork officialArtwork;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class OfficialArtwork {
    private String frontDefault;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class StatWrapper {
    private int baseStat;
    private int effort;
    private Stat stat;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
 class Stat {
    private String name;
    private String url;
}

