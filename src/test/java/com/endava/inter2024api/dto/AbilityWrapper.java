package com.endava.inter2024api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AbilityWrapper {
    private Ability ability;
    private boolean isHidden;
    private int slot;
}
