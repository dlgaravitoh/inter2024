package com.endava.inter2024api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AbilityDetail {
    private int id;
    private String name;
    private boolean isMainSeries;
    private Generation generation;
    private List<Name> names;
    private List<EffectEntry> effectEntries;
    private List<EffectChange> effectChanges;
    private List<FlavorTextEntry> flavorTextEntries;
    private List<PokemonEntry> pokemon;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Generation {
        private String name;
        private String url;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Name {
        private String name;
        private Language language;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class EffectEntry {
        private String effect;
        private String shortEffect;
        private Language language;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class EffectChange {
        private VersionGroup versionGroup;
        private List<EffectEntry> effectEntries;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class VersionGroup {
        private String name;
        private String url;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class FlavorTextEntry {
        private String flavorText;
        private Language language;
        private VersionGroup versionGroup;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class PokemonEntry {
        private boolean isHidden;
        private int slot;
        private Pokemon pokemon;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Pokemon {
        private String name;
        private String url;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Language {
        private String name;
        private String url;
    }
}
