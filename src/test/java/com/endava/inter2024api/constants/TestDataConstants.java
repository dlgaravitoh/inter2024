package com.endava.inter2024api.constants;

public final class TestDataConstants {
    //DATA NAME
    public static final String SPRIGATITO = "sprigatito";
    //GET POKEMON PATHS
    public static final String NAME = "name";
    public static final String HEIGHT = "height";
    public static final String ABILITY_NAME = "abilities.ability.name";
    public static final String PK_TYPE_NAME = "types.type.name";
    public static final String STATS_INDEX0 = "stats[0]";
    public static final String BASE_STAT = "base_stat";
    public static final String PK_SPECIES = "species";
    public static final String WEIGHT = "weight";

    //GET POKEMON ABILITY PATHS
    public static final String PK_NAME="pokemon.pokemon.name";

    private TestDataConstants() {
        throw new IllegalStateException("Utility class");
    }
}

