package com.endava.inter2024api.api.response.impl;

import com.endava.inter2024api.api.response.ApiResponse;
import io.restassured.response.Response;

public class PokeResponse<T> implements ApiResponse {
    private T data;
    private Response response;

    public PokeResponse(T data, Response response) {
        this.data = data;
        this.response = response;
    }

    public T getData() {
        return data;
    }

    public Response getResponse() {
        return response;
    }
}
