package com.endava.inter2024api.api.response;

import io.restassured.response.Response;

public interface ApiResponse {
    <T> T getData();
    Response getResponse();
}
