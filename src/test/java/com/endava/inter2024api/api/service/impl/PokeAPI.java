package com.endava.inter2024api.api.service.impl;

import com.endava.inter2024api.api.response.impl.PokeResponse;
import com.endava.inter2024api.api.service.PokemonService;
import com.endava.inter2024api.dto.AbilityDetail;
import com.endava.inter2024api.dto.Pokemon;
import com.endava.utils.ConfigManager;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class PokeAPI implements PokemonService {
    private static final String BASE_URI = ConfigManager.getProperty("baseApiEndPoint");
    private static final String GET_POKEMON_URI=ConfigManager.getProperty("getPokemon");
    private static final String GET_ABILITY_URI=ConfigManager.getProperty("getPkAbility");

    @Override
    public PokeResponse<Pokemon> getPokemon(String name) {
        Response response = given().baseUri(BASE_URI).when().get(GET_POKEMON_URI+name);
        return new PokeResponse<>(response.as(Pokemon.class), response);
    }

    @Override
    public PokeResponse<AbilityDetail> getAbility(String name) {
        Response response = given().baseUri(BASE_URI).when().get(GET_ABILITY_URI+name);
        return new PokeResponse<>(response.as(AbilityDetail.class), response);
    }

}
