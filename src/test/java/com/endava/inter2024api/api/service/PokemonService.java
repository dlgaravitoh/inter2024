package com.endava.inter2024api.api.service;

import com.endava.inter2024api.api.response.impl.PokeResponse;
import com.endava.inter2024api.dto.AbilityDetail;
import com.endava.inter2024api.dto.Pokemon;

public interface PokemonService {

    PokeResponse<Pokemon> getPokemon(String name);

    PokeResponse<AbilityDetail> getAbility(String name);
}
