package com.endava.inter2024api.api.validation;

public enum ValidationType {
    EQUALS,
    HAS_ITEM,
    GREATER_THAN,
    LESS_THAN,
    HAS_KEY,
    HAS_ENTRY,
    BOTH_GREATER_AND_LESS
}
