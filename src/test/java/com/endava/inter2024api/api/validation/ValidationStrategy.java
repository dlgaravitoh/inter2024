package com.endava.inter2024api.api.validation;

import com.endava.inter2024api.api.response.ApiResponse;
import java.util.function.Consumer;

public interface ValidationStrategy {
    void validate(ApiResponse response, String path, ValidationType type,Object expectedValue);
    void status(ApiResponse response, int status);
    void validateAll(ApiResponse response, Consumer<ApiResponse>... validations);
}

