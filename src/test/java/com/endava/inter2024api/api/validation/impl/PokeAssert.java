package com.endava.inter2024api.api.validation.impl;

import com.endava.inter2024api.api.response.ApiResponse;
import com.endava.inter2024api.api.validation.ValidationStrategy;
import com.endava.inter2024api.api.validation.ValidationType;
import io.restassured.response.ValidatableResponse;
import org.hamcrest.Matcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static org.hamcrest.Matchers.*;

public class PokeAssert implements ValidationStrategy{

    @Override
    public void validate(ApiResponse response, String path, ValidationType type,Object expectedValue) {
        ValidatableResponse validateThat = response.getResponse().then().assertThat();
        switch (type) {
            case EQUALS:
                validateThat.body(path, equalTo(expectedValue));
                break;
            case HAS_ITEM:
                validateThat.body(path, hasItem(expectedValue));
                break;
            case GREATER_THAN:
                validateThat.body(path, greaterThan((Comparable) expectedValue));
                break;
            case LESS_THAN:
                validateThat.body(path, lessThan((Comparable) expectedValue));
                break;
            case HAS_KEY:
                validateThat.body(path, hasKey(expectedValue.toString()));
                break;
            case HAS_ENTRY:
                Map.Entry<?, ?> entry = (Map.Entry<?, ?>) expectedValue;
                validateThat.body(path, hasEntry(entry.getKey(), entry.getValue()));
                break;
            case BOTH_GREATER_AND_LESS:
                Matcher<?> bounds = both(greaterThan((Comparable) ((Map.Entry) expectedValue).getKey()))
                        .and(lessThan((Comparable) ((Map.Entry) expectedValue).getValue()));
                response.getResponse().then().assertThat().body(path, bounds);
                break;
            default:
                throw new IllegalArgumentException("Invalid validation type");
        }
    }

    @Override
    public void status(ApiResponse response, int status) {
        response.getResponse().then().assertThat().statusCode(status);
    }

    @Override
    public void validateAll(ApiResponse response, Consumer<ApiResponse>... validations) {
        List<AssertionError> errors = new ArrayList<>();
        for (Consumer<ApiResponse> validation : validations) {
            try {
                validation.accept(response);
            } catch (AssertionError e) {
                errors.add(e);
            }
        }
        if (!errors.isEmpty()) {
            AssertionError combined = new AssertionError("Multiple validation errors occurred:");
            errors.forEach(combined::addSuppressed);
            throw combined;
        }
    }
}
