package com.endava.inter2024gui.test.login;

import com.endava.inter2024gui.dto.UIMessage;
import com.endava.inter2024gui.dto.User;
import com.endava.inter2024gui.page.DashBoardPage;
import com.endava.inter2024gui.page.LoginPage;
import com.endava.utils.ConfigManager;
import com.endava.utils.DataReader;
import com.endava.inter2024gui.utils.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;

import static com.endava.inter2024gui.constants.TestDataConstants.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginIT {
    private WebDriver driver;
    private WebDriverManager webDriverManager;
    private final DataReader result = new DataReader(ConfigManager.getProperty("loginTestResult"));
    private final DataReader userData = new DataReader(ConfigManager.getProperty("loginTestData"));
    private LoginPage loginPage;

    @BeforeEach
    public void setUp() {
        webDriverManager = new WebDriverManager();
        driver = webDriverManager.getDriver();
        loginPage = new LoginPage(driver);
        driver.get(ConfigManager.getProperty("baseURL"));
    }

    @Tag(SMOKE_TEST)
    @Test
    public void SuccessLogin_happyPathEX() {
        User validUser= userData.getData(VALID_USER, User.class);
        UIMessage message = result.getData(SUCCESS_LOGIN,UIMessage.class);
        DashBoardPage dashboardPage = loginPage.successLoginUser(validUser.getUsername(), validUser.getPassword());
        assertTrue(dashboardPage.getDashboardTitle().contains(message.getMessage()));
    }

    @AfterEach
    public void tearDown() {
        webDriverManager.quitDriver();
    }
}
