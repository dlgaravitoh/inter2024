package com.endava.inter2024gui.constants;

public final class TestDataConstants {
    //tags
    public static final String SMOKE_TEST = "smoke";
    //test values
    public static final String VALID_USER = "valid_user";
    public static final String SUCCESS_LOGIN = "success_login";

    private TestDataConstants() {
        throw new IllegalStateException("Utility class");
    }
}
