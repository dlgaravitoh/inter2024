package com.endava.inter2024gui.page;

import com.endava.inter2024gui.actions.WebAction;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import javax.xml.xpath.XPath;

public class DashBoardPage {
    WebAction actor;
    private final WebDriver driver;

    @FindBy(className = "post-title")
    private WebElement postLoginTitle;

    public DashBoardPage(WebDriver driver) {
        this.driver=driver;
        this.actor=new WebAction(driver);
        PageFactory.initElements(this.driver, this);
    }

    public String getDashboardTitle() {
        return actor.getText(postLoginTitle);
    }
}
